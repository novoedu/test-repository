# -*- coding:utf-8 -*-
from flask import render_template, redirect, request, url_for, flash, make_response
from . import auth
from .forms import LoginForm,RegistrationForm,editpasswordForm
from flask.ext.login import login_user
from ..models import User
from flask.ext.login import logout_user, login_required
from .. import db
from flask.ext.login import current_user
import requests


host="https://api.kquestions.com"
@auth.route('/adminlogin', methods=['GET', 'POST'])
def adminlogin():
    form = LoginForm()
    if form.validate_on_submit():
        data={ "admin" : { "email" : form.email.data, "password" : form.password.data } }
        a=requests.post(host+'/admin/login',json=data)
        if 'access_token' in a.json().keys():

            i=1
            while(1):
                b=requests.get(host+'/admins/' + str(i)).json()
                if  b['email'] == form.email.data:
                    resp = make_response(redirect(url_for('main.admin', exhibit='users')))
                    resp.set_cookie('adminid', str(i))
                    return resp
                else:
                    i += 1


        # user = User.query.filter_by(email=form.email.data).first()
        # if user is not None and user.verify_password(form.password.data):
        #     login_user(user, form.remember_me.data)
        #     return redirect(request.args.get('next') or url_for('main.admin'))
        flash(u'用户名或密码错误')
    return render_template('auth/login.html',form=form)

@auth.route('/editpassword', methods=['GET', 'POST'])
def editpassword():
    form = editpasswordForm()
    if form.validate_on_submit():
        data={ "admin" : { "password" : form.password.data, "new_password" : form.newpassword.data } }
        id = str(request.cookies.get('adminid'))
        a=requests.post(host+'/admins/'+id+'/update_password',json=data)
        if 'id' in a.json().keys():
            flash(u'密码修改成功')
            return redirect(url_for('main.admin',exhibit='users'))
        # user = User.query.filter_by(email=form.email.data).first()
        # if user is not None and user.verify_password(form.password.data):
        #     login_user(user, form.remember_me.data)
        #     return redirect(request.args.get('next') or url_for('main.admin'))
        flash(u'原密码错误')
    return render_template('auth/login.html',form=form)
@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data,
                    username=form.username.data,
                    password=form.password.data)
        db.session.add(user)
        flash('You can now login.')
        return redirect(url_for('auth.login'))


    return render_template('auth/register.html',form=form)

@auth.route('/logout')
# @login_required #required user has log in
def logout():
    resp=make_response(redirect(url_for('main.admin',exhibit='users')))
    resp.set_cookie('adminid','')
    flash(u'你已经成功登出')
    return resp

@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.ping()
        # if not current_user.confirmed \
        #         and request.endpoint[:5] != 'auth.':
        #     return redirect(url_for('auth.unconfirmed'))




