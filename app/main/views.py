# -*- coding:utf-8 -*-
from datetime import datetime
from flask import render_template, session, redirect, url_for,abort,flash,request,current_app,\
make_response,jsonify,send_file

from . import main
from .forms import NameForm,EditProfileForm,EditProfileAdminForm,PostForm,CommentForm,addclass,addchapter,addlesson,addbutton,addbutton2,addbutton3 \
    ,editclass,editchapter,editlesson,editart_program,editart_section,editart_content,addart_program,addart_section,addart_content,addvideo,adduser,edituser\
    ,addcomment, editcomment, addtag, edittag, addconsumer, editconsumer
from .. import db
from ..models import User,Role,Permission,Post,Comment
from flask.ext.login import logout_user, login_required
from flask.ext.login import current_user
from ..decorators import admin_required,permission_required
import requests,json
from ..myqiniupic import myupload
import os,uuid,base64
from requests_toolbelt import MultipartEncoder
from requests_toolbelt.utils import formdata

# host="http://123.207.235.79:3000"
host="https://api.kquestions.com"


@main.route('/', methods=['GET', 'POST'])
def index():
    form = PostForm()
    if current_user.can(Permission.WRITE_ARTICLES) and \
            form.validate_on_submit():
        post = Post(body=form.body.data,
                    author=current_user._get_current_object())
        db.session.add(post)

            # ...
        return redirect(url_for('.index')) #attention it is .index not index
    show_followed = False
    if current_user.is_authenticated:
        show_followed = bool(request.cookies.get('show_followed',''))
    if show_followed:
        query = current_user.followed_posts
    else:
        query = Post.query

    page = request.args.get('page', 1, type=int)
    pagination = query.order_by(Post.timestamp.desc()).paginate(
            page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
            error_out=False)
    posts = pagination.items
    return render_template('index.html')


# @main.route('/admin')
# def admin():
#     users_trigger=False
#     roles_trigger=False
#     users_trigger = bool(request.cookies.get('users_trigger', ''))
#     roles_trigger = bool(request.cookies.get('roles_trigger', ''))
#     return render_template('admin.html',users_trigger=users_trigger,roles_trigger=roles_trigger)
# @main.route('/admin/users')
# def admin_users():
#     resp=make_response(redirect(url_for('.admin')))
#     resp.set_cookie('users_trigger','1')
#     resp.set_cookie('roles_trigger','')
#     return resp
# @main.route('/admin/roles')
# def admin_roles():
#     resp = make_response(redirect(url_for('.admin')))
#     resp.set_cookie('users_trigger', '')
#     resp.set_cookie('roles_trigger', '1')
#     return resp



@main.route('/articles/<exhibit>',methods=['POST','GET'])
def articles(exhibit):
    delete = request.args.getlist('delete')
    program = request.args.get('program', -1, type=int)
    section = request.args.get('section', -1, type=int)
    content = request.args.get('content', -1, type=int)
    contents = request.args.get('contents', -1, type=int)
    edit = request.args.getlist('edit')




    # ---------------add program(project)----------------#
    form1 = addart_program()
    if form1.submit.data and form1.validate_on_submit():


        uid = str(uuid.uuid1())
        iconurl="http://oh8c4fk40.bkt.clouddn.com/"+"programicon_"+uid
        bannerurl="http://oh8c4fk40.bkt.clouddn.com/"+"programbanner_"+uid
        if form1.icon.data:
            iconfile = form1.icon.data.save('app/static/icons/' + form1.text.data + '.ico')
            myupload.upload("programicon_" + uid, 'app/static/icons/' + form1.text.data + '.ico')
        if form1.banner.data:
            bannerfile = form1.banner.data.save('app/static/banners/' + form1.text.data + '.ico')
            myupload.upload("programbanner_" + uid, 'app/static/banners/' + form1.text.data + '.ico')
        data = { "project" : { "name" : form1.text.data, "icon" : iconurl if form1.icon.data else "none" , "banner" : bannerurl if form1.banner.data else "none"} }

        b = requests.post(host+"/projects/create", json=data)
        return redirect(url_for('.articles', exhibit='program',program=-1))

    # ---------------add  section----------------#
    form2 = addart_section()
    if form2.submit2.data and form2.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionicon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionbanner_" + uid
        if form2.icon2.data:
            iconfile = form2.icon2.data.save('app/static/icons/' + form2.text2.data + '.ico')
            myupload.upload("sectionicon_" + uid, 'app/static/icons/' + form2.text2.data + '.ico')
        if form2.banner2.data:
            bannerfile = form2.banner2.data.save('app/static/banners/' + form2.text2.data + '.ico')
            myupload.upload("sectionbanner_" + uid, 'app/static/banners/' + form2.text2.data + '.ico')
        data = { "section" : {"name": form2.text2.data, "icon": iconurl if form2.icon2.data else "none",
			    "banner": bannerurl if form2.banner2.data else "none", "project_id": program } }

        c = requests.post(host+"/sections/create", json=data)
        return redirect(url_for('.articles', exhibit='program', program=program))
    # ---------------add content----------------#
    form8 = addart_content()
    if form8.submit3.data and form8.validate_on_submit():
        data = { "content" : { "index": 2220, "name": form8.text3.data, "ctype": 1,
			    "url": form8.texturl.data, "section_id": section } }#todo:url and ctype?

        b = requests.post(host+"/contents/create", json=data)
        return redirect(url_for('.articles', exhibit='program', program=program, section=section))

    # ---------------get all programs(projects)----------------#
    a = requests.get(host+'/projects')
    sections = None
    programs = [[i['name'], i['id'], 'program',i['icon'],i['banner']] for i in a.json()['records']]

    # ---------------get all  sections----------------#
    if program != -1:
        id = str(program)
        d = requests.get(host+'/projects/' + id + '/sections')
        sections = [[i['name'], i['id'], 'section',i['icon'],i['banner']] for i in d.json()['records']]
    # ---------------get all contents----------------#
    if section != -1:
        id = str(section)
        d = requests.get(host+'/projects/' + id + '/sections/' + id + '/contents')
        contents = [[i['name'], i['id'], 'content', i['ctype'], i['url']] for i in d.json()['records'] if i['section_id'] == section]
    # ---------------show all lessons----------------#
    if exhibit == 'content':
        id = '1'
        d = requests.get(host+'/projects/' + id + '/sections/' + id + '/contents')
        contents = [[i['name'], i['id'], 'lesson', i['ctype'], i['url']] for i in d.json()['records']]
    # ---------------edit class form----------------#
    form5 = editart_program()
    if form5.submitedit.data and form5.validate_on_submit():

        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programicon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programbanner_" + uid
        if form5.iconedit.data:
            iconfile = form5.iconedit.data.save('app/static/icons/' + form5.textedit.data + '.ico')
            myupload.upload("programicon_" +uid,'app/static/icons/' + form5.textedit.data + '.ico')
        if form5.banneredit.data:
            bannerfile = form5.banneredit.data.save('app/static/banners/' + form5.textedit.data + '.ico')
            myupload.upload("programbanner_" + uid, 'app/static/banners/' + form5.textedit.data + '.ico')
        data = {"project": {"name": form5.textedit.data, "icon": iconurl if form5.iconedit.data else [i[3] for i in programs if i[1] == program][0],
                            "banner": bannerurl if form5.banneredit.data else [i[4] for i in programs if i[1] == program][0]}}



        editid = edit[1]
        edit_column = requests.post(host+'/projects' + '/' + editid + '/update', json=data)


        return redirect(url_for('.articles', exhibit='program',program=-1))
    if program != -1 and edit:
        form5.textedit.data = [i[0] for i in programs if i[1] == program][0]
    # ---------------edit section form----------------#
    form6 = editart_section()
    if form6.submit2edit.data and form6.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionicon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionbanner_" + uid
        if form6.icon2edit.data:
            iconfile = form6.icon2edit.data.save('app/static/icons/' + form6.text2edit.data + '.ico')
            myupload.upload("sectionicon_" + uid, 'app/static/icons/' + form6.text2edit.data + '.ico')
        if form6.banner2edit.data:
            bannerfile = form6.banner2edit.data.save('app/static/banners/' + form6.text2edit.data + '.ico')
            myupload.upload("sectionbanner_" + uid, 'app/static/banners/' + form6.text2edit.data + '.ico')
        data = { "section" : {"name": form6.text2edit.data, "icon": iconurl if form6.icon2edit.data else [i[3] for i in sections if i[1] == section][0],
                "banner": bannerurl if form6.banner2edit.data else [i[4] for i in sections if i[1] == section][0], "project_id": program}}


        editid = edit[1]
        edit_section = requests.post(host +'/sections' + '/' + editid + '/update', json=data)
        return redirect(url_for('.articles', exhibit='program', program=program))
    if section != -1 and edit:
        form6.text2edit.data = [i[0] for i in sections if i[1] == section][0]
    # ---------------edit lesson form----------------#
    form9 = editart_content()
    if form9.submit3edit.data and form9.validate_on_submit():
        data = { "content" : {  "name": form9.text3edit.data, "ctype": 1,
                                "url": form9.texturledit.data, "section_id": section}}


        editid = edit[1]
        edit_content = requests.post(host+'/contents' + '/' + editid + '/update', json=data)
        return redirect(url_for('.articles', exhibit='program', program=program, section=section))
    if content != -1 and edit:
        form9.text3edit.data = [i[0] for i in contents if i[1] == content][0]
        form9.texturledit.data = [i[4] for i in contents if i[1] == content][0]
    # ---------------delete course or section or lesson----------------#
    if delete:
        if delete[2] == 'program':
            deleteid = delete[1]
            delete_program = requests.post(host+'/projects' + '/' + deleteid + '/delete')
            return redirect(url_for('.articles', exhibit='program',program=-1))

        elif delete[2] == 'section':
            deleteid = delete[1]
            delete_section = requests.post(host+'/sections' + '/' + deleteid + '/delete')
            return redirect(url_for('.articles', exhibit='program', program=program))
        elif delete[2] == 'content':
            deleteid = delete[1]
            delete_lesson = requests.post(host+'/contents' + '/' + deleteid + '/delete')
            return redirect(url_for('.articles', exhibit='program', program=program, section=section))

    return render_template('articles.html', exhibit=exhibit, programs=programs, a=a, program=program, sections=sections,
                           section=section, contents=contents \
                           , content=content, form=form1, form2=form2,  form5=form5, form6=form6,
                            form8=form8, form9=form9)









@main.route('/courses/<exhibit>',methods=['GET','POST'])
def courses(exhibit):
    global columns,chapters
    delete = []

    delete = request.args.getlist('delete')
    column = request.args.get('column', -1, type=int)
    chapter = request.args.get('chapter', -1, type=int)
    course = request.args.get('course', -1, type=int)

    edit = request.args.getlist('edit')
    videoresp = requests.get(host + '/videos')

    videos = [i for i in videoresp.json()['records']]

    # ---------------click button to the new page where you can add course or chapter----------------#
    form3=addbutton()
    if form3.button.data and form3.validate_on_submit():

        return redirect(url_for('.courses',exhibit='addclass',column=column))
    form4=addbutton2()
    if form4.button2.data and form4.validate_on_submit():

        return redirect(url_for('.courses', exhibit='addchapter',column=column))
    form7 = addbutton3()
    if form7.button3.data and form7.validate_on_submit():
        return redirect(url_for('.courses', exhibit='addlesson', column=column,chapter=chapter))

    # ---------------add class----------------#


    form1 = addclass()
    if form1.submit.data and form1.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "courseicon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "coursebanner_" + uid
        if form1.icon.data:
            iconfile = form1.icon.data.save('app/static/icons/' + form1.text.data + '.ico')
            myupload.upload("courseicon_" + uid, 'app/static/icons/' + form1.text.data + '.ico')
        if form1.banner.data:
            bannerfile = form1.banner.data.save('app/static/banners/' + form1.text.data + '.ico')
            myupload.upload("coursebanner_" + uid, 'app/static/banners/' + form1.text.data + '.ico')
        data = {'course':
                    {'name': form1.text.data,
                     'icon': iconurl if form1.icon.data else "none",
                     'banner': bannerurl if form1.banner.data else "none",
                     'desc': form1.classdesc.data
                     }
                }



        b = requests.post(host+"/courses/create", json=data)
        return redirect(url_for('.courses', exhibit='column',column=-1))

    # ---------------add chapter----------------#

    form2 = addchapter()
    if form2.submit2.data and form2.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chaptericon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chapterbanner_" + uid
        if form2.icon2.data:
            iconfile = form2.icon2.data.save('app/static/icons/' + form2.text2.data + '.ico')
            myupload.upload("chaptericon_" + uid, 'app/static/icons/' + form2.text2.data + '.ico')
        if form2.banner2.data:
            bannerfile = form2.banner2.data.save('app/static/banners/' + form2.text2.data + '.ico')
            myupload.upload("chapterbanner_" + uid, 'app/static/banners/' + form2.text2.data + '.ico')

        data = {"chapter": {"name": form2.text2.data, "icon": iconurl if form2.icon2.data else "none", \
                            "banner": bannerurl if form2.banner2.data else "none", "course_id": column}}



        c = requests.post(host+"/chapters/create", json=data)
        return redirect(url_for('.courses', exhibit='column',column=column))
    # ---------------add lesson----------------#
    form8 = addlesson()

    if form8.submit3.data and form8.validate_on_submit():
        data = { "lesson" : {"name" : form8.text3.data, "banner" : "bbbbanner",
			    "browses" : 15, "desc" : form8.lessondesc.data, "chapter_id" : int(form8.belongchapter.data),"video_id":1 } }#todo:video_id is not all 1
        print int(form8.belongchapter.data)
        b = requests.post(host+"/lessons/create", json=data)
        print b.json()


        return redirect(url_for('.courses', exhibit='column',column=column,chapter=chapter))


    #---------------get all courses----------------#
    a = requests.get(host+'/courses')
    chapters = None
    columns = [[i['name'], i['id'], 'column',i['icon'],i['banner']] for i in a.json()['records']]

    # ---------------get all chapters----------------#
    if column != -1:
        id=str(column)
        d = requests.get(host+'/courses/' + id + '/chapters')
        chapters = [[i['name'],i['id'],'chapter',i['icon'],i['banner']] for i in d.json()['records']]
    # ---------------get all lessons----------------#
    if chapter != -1 :
        id = str(chapter)
        d = requests.get(host+'/courses/' + id + '/chapters/'+id +'/lessons')
        courses = [[i['name'], i['id'], 'lesson',i['desc'],i['browses'],i['created_at'],i['banner'],i['chapter_id'],i['course_id']] for i in d.json()['records'] if i['chapter_id']==chapter]
    # ---------------show all lessons----------------#

    id = '1'
    d = requests.get(host+'/courses/' + id + '/chapters/' + id + '/lessons')
    courses = [[i['name'], i['id'], 'lesson', i['desc'],i['browses'],i['created_at'],i['banner'],i['chapter_id'],i['course_id']] for i in d.json()['records'] ]


    # ---------------edit class form----------------#
    form5 = editclass()
    if form5.submitedit.data and form5.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "courseicon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "coursebanner_" + uid
        if form5.iconedit.data:
            iconfile = form5.iconedit.data.save('app/static/icons/' + form5.textedit.data + '.ico')
            myupload.upload("courseicon_" + uid, 'app/static/icons/' + form5.textedit.data + '.ico')
        if form5.banneredit.data:
            bannerfile = form5.banneredit.data.save('app/static/banners/' + form5.textedit.data + '.ico')
            myupload.upload("coursebanner_" + uid, 'app/static/banners/' + form5.textedit.data + '.ico')
        data = {"course": {"name": form5.textedit.data,
                            "icon": iconurl if form5.iconedit.data else [i[3] for i in columns if i[1] == column][0],
                            "banner": bannerurl if form5.banneredit.data else
                            [i[4] for i in columns if i[1] == column][0],"desc": "none"}}#todo:desc should be exhibited ?


        editid = edit[1]
        edit_column = requests.post(host+'/courses' + '/' + editid + '/update',json=data)
        print list(edit_column)

        return redirect(url_for('.courses', exhibit='column',column=-1))
    if column!=-1 and edit:
        form5.textedit.data = [i[0] for i in columns if i[1]==column][0]
    # ---------------edit chapter form----------------#
    form6 = editchapter()
    if form6.submit2edit.data and form6.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chaptericon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chapterbanner_" + uid
        if form6.icon2edit.data:
            iconfile = form6.icon2edit.data.save('app/static/icons/' + form6.text2edit.data + '.ico')
            myupload.upload("chaptericon_" + uid, 'app/static/icons/' + form6.text2edit.data + '.ico')
        if form6.banner2edit.data:
            bannerfile = form6.banner2edit.data.save('app/static/banners/' + form6.text2edit.data + '.ico')
            myupload.upload("chapterbanner_" + uid, 'app/static/banners/' + form6.text2edit.data + '.ico')
        data = {"chapter": {"name": form6.text2edit.data,
                            "icon": iconurl if form6.icon2edit.data else [i[3] for i in chapters if i[1] == chapter][0],
                            "banner": bannerurl if form6.banner2edit.data else
                            [i[4] for i in chapters if i[1] == chapter][0], "course_id": column}}


        editid = edit[1]
        edit_chapter = requests.post(host+'/chapters' + '/' + editid + '/update',json=data)
        return redirect(url_for('.courses', exhibit='column', column=column))
    if chapter != -1 and edit:
        form6.text2edit.data = [i[0] for i in chapters if i[1] == chapter][0]
    # ---------------edit lesson form----------------#
    form9 = editlesson()
    if form9.submit3edit.data and form9.validate_on_submit():
        data = {"lesson": {"name": form9.text3edit.data, "banner": "bbbbanner",
                           "browses": [i[4] for i in courses if i[1] == course][0], "desc": form9.lessondescedit.data,\
                           "chapter_id": form9.belongchapteredit.data,"course_id":column,"video_id":1}}
        #todo:browse will be send?and lessons have banner and video_id is not 1

        editid = edit[1]
        edit_chapter = requests.post(host+'/lessons' + '/' + editid + '/update', json=data)
        print list(edit_chapter)

        # elif edit[2] == 'chapter':
        #     editid = edit[1]
        #     edit_chapter = requests.post('http://192.168.1.48:3000' + '/chapters' + '/' + editid + '/update')



        return redirect(url_for('.courses', exhibit='column', column=column,chapter=chapter))
    if course != -1 and edit:
        print course
        print courses
        form9.text3edit.data = [i[0] for i in courses if i[1] == course][0]
        form9.lessondescedit.data = [i[3] for i in courses if i[1] == course][0]
        form9.belongchapteredit.data =str([i[7] for i in courses if i[1] == course][0])
    # ---------------delete course or chapter or lesson----------------#
    if delete:
        if delete[2]=='column':
            deleteid=delete[1]
            delete_column=requests.post(host+'/courses'+'/'+deleteid+'/delete')
            return redirect(url_for('.courses',exhibit='column',column=-1))

        elif delete[2]=='chapter':
            deleteid=delete[1]
            delete_chapter=requests.post(host+'/chapters'+'/'+deleteid+'/delete')
            return redirect(url_for('.courses', exhibit='column',column=column))
        elif delete[2]=='lesson':
            deleteid = delete[1]
            delete_lesson = requests.post(host+'/lessons' + '/' + deleteid + '/delete')
            return redirect(url_for('.courses', exhibit='column', column=column, chapter=chapter))





    return render_template('courses.html', exhibit=exhibit, columns=columns, a=a, column=column, chapters=chapters, chapter=chapter, courses=courses \
                           , course=course,form=form1,form2=form2,form3=form3,form4=form4,form5=form5,form6=form6,form7=form7,form8=form8,form9=form9,videos=videos)


@main.route('/media/<exhibit>',methods=['POST','GET'])
def media(exhibit):
    delete = request.args.getlist('delete')
    media = request.args.get('media', -1, type=int)
    video = request.args.get('video', -1, type=int)

    edit = request.args.getlist('edit')

    # ---------------add program(project)----------------#
    form1 = addvideo()
    if form1.submit.data and form1.validate_on_submit():


        file1=form1.video.data.save('1234.mp4')
        querystring = {"method": "uploadfile"}
        print type(file1)
        #a=jsonify(file1)
        data=MultipartEncoder({"writetoken":"4eb2be96-fed2-4728-a94d-d8b62ca36670",
               "JSONRPC":({"title":form1.videoname.data,"tag":form1.tag.data,"desc":form1.desc.data}),
              "Filedata":open('1234.mp4','rb')})
        headers = {'Content-Type': data.content_type}
        po=requests.post('http://v.polyv.net/uc/services/rest?method=uploadfile',data=data,headers=headers)
        print list(po),headers,123
    #
    #     b = requests.post("http://" + host + "/projects/create", json=data)
    #     return redirect(url_for('.articles', exhibit='program', program=-1))


    print request.form
    # ---------------get all programs(projects)----------------#
    a = requests.get( host + '/videos')

    videos = [ i for i in a.json()['records']]



    # # ---------------edit class form----------------#
    # form5 = editart_program()
    # if form5.submitedit.data and form5.validate_on_submit():
    #
    #     uid = str(uuid.uuid1())
    #     iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programicon_" + uid
    #     bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programbanner_" + uid
    #     if form5.iconedit.data:
    #         iconfile = form5.iconedit.data.save('app/static/icons/' + form5.textedit.data + '.ico')
    #         myupload.upload("programicon_" + uid, 'app/static/icons/' + form5.textedit.data + '.ico')
    #     if form5.banneredit.data:
    #         bannerfile = form5.banneredit.data.save('app/static/banners/' + form5.textedit.data + '.ico')
    #         myupload.upload("programbanner_" + uid, 'app/static/banners/' + form5.textedit.data + '.ico')
    #     data = {"project": {"name": form5.textedit.data,
    #                         "icon": iconurl if form5.iconedit.data else [i[3] for i in programs if i[1] == program][0],
    #                         "banner": bannerurl if form5.banneredit.data else
    #                         [i[4] for i in programs if i[1] == program][0]}}
    #
    #     editid = edit[1]
    #     edit_column = requests.post('http://' + host + '/projects' + '/' + editid + '/update', json=data)
    #     print list(edit_column)
    #
    #     return redirect(url_for('.articles', exhibit='program', program=-1))
    # if program != -1 and edit:
    #     form5.textedit.data = [i[0] for i in programs if i[1] == program][0]
    #
    # # ---------------delete course or section or lesson----------------#
    # if delete:
    #     if delete[2] == 'program':
    #         deleteid = delete[1]
    #         delete_program = requests.post('http://' + host + '/projects' + '/' + deleteid + '/delete')
    #         return redirect(url_for('.articles', exhibit='program', program=-1))



    return render_template('media.html',exhibit=exhibit,video=video,videos=videos,media=media,form1=form1)

@main.route('/admin/<exhibit>',methods=['GET','POST'])
def admin(exhibit):
    editid= request.args.get('editid',-1,type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    a = requests.get(host+'/users')
    users = a.json()['records']

    #: -----------------adduser-------------------#
    form1=adduser()
    if form1.submit.data and form1.validate_on_submit():
        data={"user" : {"phone":form1.phone.data, "utype":int(form1.utype.data),
                        "status":int(form1.status.data), "nick":form1.nick.data,
                        "gender":form1.gender.data,
                        "email":form1.email.data, "avatar":"avatar_url"}}
        b=requests.post(host+'/users/create',json=data)
        return redirect(url_for('.admin',exhibit='users'))

    #: -----------------edituser------------------#
    form2=edituser()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {"user": {"phone": form2.phoneedit.data, "utype": form2.utypeedit.data, "status": form2.statusedit.data, "nick": form2.nickedit.data,
                         "gender": form2.genderedit.data, "email": form2.emailedit.data, "avatar": "avatar_url"}}
        id=str(editid)
        c = requests.post(host+'/users/'+id+'/update', json=data)
        return redirect(url_for('.admin', exhibit='users'))
    if editid !=-1:
        form2.phoneedit.data=[i['phone'] for i in users if i['id']==editid][0]
        form2.nickedit.data = [i['nick'] for i in users if i['id'] == editid][0]
        form2.genderedit.data = str([i['gender'] for i in users if i['id'] == editid][0])
        form2.emailedit.data = [i['email'] for i in users if i['id'] == editid][0]
        form2.utypeedit.data = str([i['utype'] for i in users if i['id'] == editid][0])
        form2.statusedit.data = str([i['status'] for i in users if i['id'] == editid][0])
    #: ----------------deleteuser-----------------#
    if deleteid != -1:
        id = str(deleteid)
        requests.post(host+'/users/'+id+'/delete')
        return redirect(url_for('.admin', exhibit='users'))
    return render_template('admin.html',users=users,exhibit=exhibit,form1=form1,form2=form2)

@main.route('/comments/<exhibit>',methods=['GET','POST'])
def comments(exhibit):
    page = request.args.get('page', 1, type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    #: -----------------showcomments----------------#
    commentsresp = requests.get(host+'/comments?page='+str(page))
    comments = commentsresp.json()['records']
    #: -----------------addcomment----------------#
    form1 = addcomment()
    if form1.submit.data and form1.validate_on_submit():
        data = {"comment": {"title": form1.title.data, "text": form1.text.data,
                            "status": int(form1.status.data), "poster_id": 1, "lesson_id": 2, "content_id": 3}}
        requests.post(host+'/comments/create', json=data)
        return redirect(url_for('.comments', exhibit='show_comments'))
    #: -----------------editcomment----------------#
    form2 = editcomment()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {"comment": {"title": form2.titleedit.data, "text": form2.textedit.data,
                            "status": int(form2.statusedit.data), "poster_id": 1, "lesson_id": 2, "content_id": 3}}
        if editid != -1:
            id = str(editid)
            requests.post(host+'/comments/'+id+'/update', json=data)
            return redirect(url_for('.comments', exhibit='show_comments', page=page))
    if editid != -1:
        form2.titleedit.data = [i['title'] for i in comments if i['id'] == editid][0]
        form2.textedit.data = [i['text'] for i in comments if i['id'] == editid][0]
        form2.statusedit.data = str([i['status'] for i in comments if i['id'] == editid][0])
    #: -----------------deletecomment----------------#
    if deleteid != -1:
        id = str(deleteid)
        requests.post(host+'/comments/'+id+'/delete')
        return redirect(url_for('.comments', exhibit='show_comments', page=page))
    return render_template('comments.html', exhibit=exhibit, comments=comments, commentsresp=commentsresp, page=page,\
                           form1=form1, form2=form2)

@main.route('/tags/<exhibit>', methods=['GET', 'POST'])
def tags(exhibit):
    page = request.args.get('page', 1, type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    tagsresp = requests.get(host+'/tags?page='+str(page))
    tags = tagsresp.json()['records']

    #: -----------------addtag----------------#
    form1 = addtag()
    if form1.submit.data and form1.validate_on_submit():
        data = {"tag": {"name": form1.name.data}}
        a= requests.post(host+'/tags/create', json=data)
        return redirect(url_for('.tags', exhibit='show_tags'))
    #: -----------------edittag----------------#
    form2 = edittag()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {"tag" : {"name" : form2.nameedit.data }}
        if editid != -1:
            id = str(editid)
            a = requests.post(host+'/tags/'+id+'/update', json=data)
            return redirect(url_for('.tags', exhibit='show_tags', page=page))
    if editid != -1:
        form2.nameedit.data = [i['name'] for i in tags if i['id'] == editid][0]

    #: -----------------deletetag----------------#
    if deleteid != -1:
        id = str(deleteid)
        a = requests.post(host+'/tags/'+id+'/delete')
        return redirect(url_for('.tags', exhibit='show_tags', page=page))
    return render_template('tags.html', exhibit=exhibit, tagsresp=tagsresp, tags=tags, form1=form1, page=page, form2=form2)


@main.route('/consumers/<exhibit>', methods=['GET', 'POST'])
def consumers(exhibit):
    page = request.args.get('page', 1, type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    #: -----------------showconsumers----------------#
    consumersresp = requests.get(host + '/consumers?page=' + str(page))
    consumers = consumersresp.json()['records']
    #: -----------------addconsumer----------------#
    form1 = addconsumer()
    if form1.submit.data and form1.validate_on_submit():
        data = {"consumer": {"name": form1.name.data,
                             "status": int(form1.status.data)}}
        requests.post(host+'/consumers/create', json=data)
        return redirect(url_for('.consumers', exhibit='show_consumers', page=page))
    #: -----------------editconsumer----------------#
    form2 = editconsumer()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {"consumer": {"name": form2.nameedit.data,
                             "status": int(form2.statusedit.data)}}
        if editid != -1:
            id = str(editid)
            requests.post(host+'/consumers/'+id+'/update', json=data)
            return redirect(url_for('.consumers', exhibit='show_consumers', page=page))
    if editid != -1:
        form2.nameedit.data = [i['name'] for i in consumers if i['id'] == editid][0]
        form2.statusedit.data = str([i['status'] for i in consumers if i['id'] == editid][0])

    #: -----------------deleteconsumer----------------#
    if deleteid != -1:
        id = str(deleteid)
        requests.post(host+'/consumers/'+id+'/delete')
        return redirect(url_for('.consumers', exhibit='show_consumers', page=page))
    return render_template('consumers.html', exhibit=exhibit, consumersresp=consumersresp, consumers=consumers, page=page,
                           form1=form1, form2=form2)

@main.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(404)
    posts = user.posts.order_by(Post.timestamp.desc()).all()
    return render_template('user.html', user=user,posts=posts)

@main.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        db.session.add(current_user) #current_user must be in db, add() can modify data
        flash('Your profile has been updated.')
        return redirect(url_for('.user', username=current_user.username))
    form.name.data = current_user.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', form=form)

@main.route('/edit-profile/<int:id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_profile_admin(id):
    user = User.query.get_or_404(id)
    form = EditProfileAdminForm(user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.confirmed = form.confirmed.data
        user.role = Role.query.get(form.role.data)
        user.name = form.name.data
        user.location = form.location.data
        user.about_me = form.about_me.data
        db.session.add(user)
        flash('The profile has been updated.')
        return redirect(url_for('.user', username=user.username))
    form.email.data = user.email
    form.username.data = user.username
    form.confirmed.data = user.confirmed
    form.role.data = user.role_id
    form.name.data = user.name
    form.location.data = user.location
    form.about_me.data = user.about_me
    return render_template('edit_profile.html', form=form, user=user)

@main.route('/post/<int:id>',methods=['GET','POST'])
def post(id):
    post=Post.query.get_or_404(id)
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(body = form.body.data,
                          post=post,
                          author=current_user._get_current_object())
        db.session.add(comment)
        flash('Your comment has been published.')
        return redirect(url_for('.post',id=post.id,page=-1))
    page= request.args.get('page',1,type=int)
    if page == -1:
        page=(post.comments.count() -1)/ \
            current_app.config['FLASKY_COMMENTS_PER_PAGE']+1
    pagination = post.comments.order_by(Comment.timestamp.asc()).paginate(
        page,per_page=current_app.config['FLASKY_COMMENTS_PER_PAGE'],
        error_out=False)
    comments = pagination.items

    return render_template('post.html',posts=[post],form=form,comments=comments,
                           pagination=pagination)

@main.route('/edit/<int:id>',methods=['GET','POST'])
@login_required
def edit(id):
    post=Post.query.get_or_404(id)
    if current_user!=post.author and \
        not current_user.can(Permission.ADMINISTER):
        abort(403)
    form=PostForm()
    if form.validate_on_submit():
        post.body=form.body.data
        db.session.add(post)
        flash('The post has been updated.')
        return redirect(url_for('.post',id=post.id))
    form.body.data = post.body
    return render_template('edit_post.html',form=form)

@main.route('/follow/<username>')
@login_required
@permission_required(Permission.FOLLOW)
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user')
        return redirect(url_for('.index'))
    if current_user.is_following(user):
        flash('You are already following this user.')
        return redirect(url_for(',user',username=username))
    current_user.follow(user)
    flash('You are now following %s.'%username)
    return redirect(url_for('.user',username=username))

@main.route('/unfollow/<username>')
@login_required
@permission_required(Permission.FOLLOW)
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user')
        return redirect(url_for('.index'))
    if not current_user.is_following(user):
        flash('You are not following this user.')
        return redirect(url_for(',user',username=username))
    current_user.unfollow(user)
    flash('You are now not following %s.'%username)
    return redirect(url_for('.user',username=username))

@main.route('/followers/<username>')
def followers(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    page = request.args.get('page',1,type=int)
    pagination = user.followers.paginate(
        page,per_page=current_app.config['FLASKY_FOLLOWERS_PER_PAGE'],
        error_out=False)
    follows = [{'user':item.follower,'timestamp':item.timestamp}
               for item in pagination.items]
    return render_template('followers.html',user=user,title='Followers of',
                           endpoint='.followers',pagination=pagination,
                           follows=follows)
@main.route('/followed_by/<username>')
def followed_by(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    page = request.args.get('page',1,type=int)
    pagination = user.followed.paginate(
        page,per_page=current_app.config['FLASKY_FOLLOWED_PER_PAGE'],
        error_out=False)
    follows = [{'user':item.followed,'timestamp':item.timestamp}
               for item in pagination.items]
    return render_template('followers.html',user=user,title='Followed by',
                           endpoint='.followed_by',pagination=pagination,
                           follows=follows)

@main.route('/all')
@login_required
def show_all():
    resp =make_response(redirect(url_for('.index')))
    resp.set_cookie('show_followed','',max_age=30*24*60*60)
    return resp

@main.route('/followed')
@login_required
def show_followed():
    resp =make_response(redirect(url_for('.index')))
    resp.set_cookie('show_followed','1',max_age=30*24*60*60)
    return resp

@main.route('/moderate')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate():
    page = request.args.get('page',1,type=int)
    pagination = Comment.query.order_by(Comment.timestamp.desc()).paginate(
        page,per_page=current_app.config['FLASKY_COMMENTS_PER_PAGE'],
        error_out=False)
    comments = pagination.items
    return render_template('moderate.html',comments=comments,
                           pagination=pagination,page=page)
@main.route('/moderate/enable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_enable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled=False
    db.session.add(comment)
    return redirect(url_for('.moderate',page=request.args.get('page',1,type=int)))

@main.route('/moderate/disable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_disable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled=True
    db.session.add(comment)
    return redirect(url_for('.moderate',page=request.args.get('page',1,type=int)
                            ))
