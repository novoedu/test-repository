# -*- coding:utf-8 -*-
from flask_wtf import Form,widgets

from wtforms import StringField, TextAreaField, BooleanField, SelectField,\
    SubmitField

from wtforms.validators import Required, Length, Email, Regexp
from wtforms import ValidationError
from ..models import Role, User, Post
from flask.ext.pagedown.fields import PageDownField
import requests

from flask_wtf.file import FileField,FileAllowed
from flask_wtf.html5 import URLField
from flaskckeditor import CKEditor
host="https://api.kquestions.com"
class NameForm(Form):
    name = StringField('What is your name?', validators=[Required()])
    submit = SubmitField('Submit')


class EditProfileForm(Form):
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')


class EditProfileAdminForm(Form):
    email = StringField('Email', validators=[Required(), Length(1, 64),
                                             Email()])
    username = StringField('Username', validators=[
        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Usernames must have only letters, '
                                          'numbers, dots or underscores')])
    confirmed = BooleanField('Confirmed')
    role = SelectField('Role', coerce=int)
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')

    def __init__(self, user, *args, **kwargs):
        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
        self.role.choices = [(role.id, role.name)
                             for role in Role.query.order_by(Role.name).all()]
        self.user = user

    def validate_email(self, field):
        if field.data != self.user.email and \
                User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

    def validate_username(self, field):
        if field.data != self.user.username and \
                User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already in use.')


class PostForm(Form):
    body = PageDownField("what is your opinion?", validators=[Required()])
    submit = SubmitField('submit')


class CommentForm(Form):
    body = StringField('',validators=[Required()])
    submit = SubmitField('Submit')


class addclass(Form,CKEditor):
    text = StringField(u'课类名',validators=[Length(1, 64)])
    icon = FileField(u'图标', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    banner = FileField(u'图像', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    ckdemo= TextAreaField()
    classtag=StringField(u'课类标签')
    classdesc=TextAreaField(u'摘要')
    submit = SubmitField(u'提交')


class addchapter(Form,):
    text2 = StringField(u'章节名', validators=[Length(1, 64)])
    icon2 = FileField(u'图标', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    banner2 = FileField(u'图像', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    submit2 = SubmitField(u'提交')


class addlesson(Form,CKEditor):
    text3 = StringField(u'课程名')
    ckdemo = TextAreaField()
    id1=[]
    for i in requests.get(host+"/courses").json()['records']:
        id1.append(i['id'])
    id2=[]
    for j in id1:
        id2+=requests.get(host + "/courses/" + str(j) + "/chapters").json()['records']
    id3=[]
    for k in id2:
        m=(str(k['id']),k['name'])
        id3.append(m)
    belongchapter= SelectField(u'所属章节',choices=id3)
    lessontag=StringField(u'课程标签')
    lessondesc=TextAreaField(u'摘要')
    submit3 = SubmitField(u'提交')


class addbutton(Form):
    button = SubmitField(u'+新建课程门类')


class addbutton2(Form):
    button2 = SubmitField(u'新建课程章节+')


class addbutton3(Form):
    button3 = SubmitField(u'录入课程+')


class editclass(Form):
    textedit = StringField(u'课类名', validators=[Length(1, 64)])
    iconedit = FileField(u'图标', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    banneredit = FileField(u'图像', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    submitedit = SubmitField(u'提交')


class editchapter(Form):
    text2edit = StringField(u'章节名', validators=[Length(1, 64)])
    icon2edit = FileField(u'图标', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    banner2edit = FileField(u'图像', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    submit2edit = SubmitField(u'提交')


class editlesson(Form,CKEditor):
    text3edit = StringField(u'课程名', validators=[Length(1, 64)])
    ckdemoedit = TextAreaField()
    id1 = []
    for i in requests.get(host + "/courses").json()['records']:
        id1.append(i['id'])
    id2 = []
    for j in id1:
        id2 += requests.get(host + "/courses/" + str(j) + "/chapters").json()['records']
    id3 = []
    for k in id2:
        m = (str(k['id']), k['name'])
        id3.append(m)
    belongchapteredit = SelectField(u'所属章节', choices=id3)
    lessontagedit = StringField(u'课程标签')
    lessondescedit = TextAreaField(u'摘要')
    submit3edit = SubmitField(u'提交')


class addart_program(Form):
    text = StringField(u'栏目名', validators=[Length(1, 64)])
    icon = FileField(u'图标',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    banner = FileField(u'图像',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    submit = SubmitField(u'提交')


class addart_section(Form):
    text2 = StringField(u'章节名', validators=[Length(1, 64)])
    icon2 = FileField(u'图标',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    banner2 = FileField(u'图像',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    submit2 = SubmitField(u'提交')


class addart_content(Form):
    text3 = StringField(u'微内容名',validators=[Length(1, 64)])
    texturl= URLField(u'url(例如http://www.baidu.com/)', validators=[Length(1, 64)])
    submit3 = SubmitField(u'提交')


class editart_program(Form):
    textedit = StringField(u'栏目名', validators=[Length(1, 64)])
    iconedit = FileField(u'图标',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    banneredit = FileField(u'图像',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    submitedit = SubmitField(u'提交')
class editart_section(Form):
    text2edit = StringField(u'章节名', validators=[Length(1, 64)])
    icon2edit = FileField(u'图标',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    banner2edit = FileField(u'图像',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    submit2edit = SubmitField(u'提交')
class editart_content(Form):
    text3edit = StringField(u'微内容名', validators=[Length(1, 64)])
    texturledit = URLField(u'url(例如http://www.baidu.com/)', validators=[Length(1, 64)])


    submit3edit = SubmitField(u'提交')

class addvideo(Form):
    videoname= StringField(u'视频名', validators=[Length(1, 64)])
    tag=StringField(u'视频标签', validators=[Length(0, 64)])
    desc=StringField(u'视频描述')
    video=FileField(u'视频',validators=[FileAllowed(['flv','avi','mpg','mp4','wmv','mov','3gp','asf'],u"只能上传'flv','avi','mpg','mp4','wmv','mov','3gp','asf'格式的视频")])
    submit=SubmitField(u'提交')


class adduser(Form):
    nick = StringField(u'昵称', validators=[Length(1, 64)])
    phone=StringField(u'手机号',validators=[Length(1, 64)])
    email = StringField(u'邮箱', validators=[Required(), Length(1, 64),
                                             Email()])
    avatar = FileField(u'头像',validators=[FileAllowed(['ico','jpg','png'],u"只能上传'.ico','.png','.jpg'类型的图片")])
    gender = SelectField(u'性别',choices=[('0',u'男'),('1',u'女')])
    utype = SelectField(u'性别',choices=[('0',u'学生'),('1',u'老师')])
    status = SelectField(u'性别',choices=[('0',u'正常'),('1',u'封禁')])

    submit=SubmitField(u'提交')


class edituser(Form):
    nickedit = StringField(u'昵称', validators=[Length(1, 64)])
    phoneedit = StringField(u'手机号', validators=[Length(1, 64)])
    emailedit = StringField(u'邮箱', validators=[Required(), Length(1, 64),
                                           Email()])
    avataredit = FileField(u'头像', validators=[FileAllowed(['ico', 'jpg', 'png'], u"只能上传'.ico','.png','.jpg'类型的图片")])
    genderedit = SelectField(u'性别', choices=[('0', u'男'), ('1', u'女')])
    utypeedit = SelectField(u'用户类别', choices=[('0', u'学生'), ('1', u'老师')])
    statusedit = SelectField(u'用户状态', choices=[('0', u'正常'), ('1', u'封禁')])

    submitedit = SubmitField(u'提交')


class addcomment(Form):
    title=StringField(u'评论标题', validators=[Length(1, 64)])
    text = TextAreaField(u'评论内容')
    status=SelectField(u'审核状态',choices=[('0',u'未审核'),('1',u'已审核'),('2',u'未显示')])
    submit = SubmitField(u'提交')


class editcomment(Form):
    titleedit = StringField(u'评论标题', validators=[Length(1, 64)])
    textedit = TextAreaField(u'评论内容')
    statusedit = SelectField(u'审核状态',choices=[('0',u'未审核'),('1',u'已审核'),('2',u'未显示')])
    submitedit = SubmitField(u'提交')


class addtag(Form):
    name = StringField(u'标签名', validators=[Length(1, 64)])
    submit = SubmitField(u'提交')


class edittag(Form):
    nameedit = StringField(u'标签名', validators=[Length(1, 64)])
    submitedit = SubmitField(u'提交')


class addconsumer(Form):
    name = StringField(u'客户名', validators=[Length(1, 64)])
    status = SelectField(u'审核状态',choices=[('0',u'未启用'),('1',u'已启用'),('2',u'已禁用')])
    submit = SubmitField(u'提交')



class editconsumer(Form):
    nameedit = StringField(u'客户名', validators=[Length(1, 64)])
    statusedit = SelectField(u'审核状态',choices=[('0',u'未启用'),('1',u'已启用'),('2',u'已禁用')])
    submitedit = SubmitField(u'提交')
