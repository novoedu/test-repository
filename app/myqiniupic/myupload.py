# -*- coding: utf-8 -*-
# flake8: noqa

from qiniu import Auth, put_file, etag, urlsafe_base64_encode
import qiniu.config
import os

#需要填写你的 Access Key 和 Secret Key
access_key = '1WCJfXWbaq4IQD9rhsgF5Mi-JCfh79xsUJUg3n5s'
secret_key = '1AJcUomeRSECeDxhbArWyqhdha_ve2gy7Px1SGrL'

#构建鉴权对象
q = Auth(access_key, secret_key)

#要上传的空间
bucket_name = 'test'
def upload(name,path):
    #上传到七牛后保存的文件名
    key = name

    #生成上传 Token，可以指定过期时间等
    token = q.upload_token(bucket_name, key, 3600)

    #要上传文件的本地路径
    localfile = path

    ret, info = put_file(token, key, localfile)
    print(info)
    assert ret['key'] == key
    assert ret['hash'] == etag(localfile)
    os.remove(path)

